pil = int(input('Silahkan Pilih Yang Ingin Anda Cari\n1-Sequential Search\n2-Binary Search\n>>> '))
if pil == 1:
    print("1-Sequential Search")
    def sq(data,key):
      pos=[]
      for i in range(len(data)):
          if data[i] == key:
              pos.append(i+1)
      if len(pos) > 0:
              print(" >> Data", key, "Ditemukan Sebanyak", len(pos),  "Kali Di Posisi",pos)
      else:
          print(" >> Data Tidak Ditemukan !!!")
      return pos

    import json
    file_json = open("data.json")
    data = json.loads(file_json.read())
    sq(data["dataSq"],15)
    sq(data["dataSq"],90)
    
elif pil == 2:
    print("2-Binary Search")
    def bs(data, key):
        awal = 1
        akhir = len(data)+1
        ketemu = False
        while (awal <= akhir) and not ketemu:
            tengah = int((awal + akhir)/2)
            if key == data[tengah]:
                ketemu = True
                print(" >> Data", key, "Ditemukan Diposisi", tengah + 1)
            elif key<data[tengah]:
                akhir = tengah - 1
            else:
                awal = tengah + 1
        if not ketemu:
            print(" >> Data Tidak Ditemukan !!!")

    import json
    file_json = open("data.json")
    data = json.loads(file_json.read())
    bs(data["dataBs"],23)
else:
    print("Maaf Yang Anda Cari Tidak Ada")
